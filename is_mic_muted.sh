#! /bin/bash

pacmd list-sources | awk '\
BEGIN {default_found=0;}

/^[\t ]*\*/ {default_found=1;}

/^[\t ]*name:/ {
    if (default_found) {
        name=$2;
        gsub("[<>]", "", name);
    }
}

/^[\t ]*muted:/ {
    if (default_found) {
        if ($2=="yes") {
           exit 0
        } else {
            exit 1
        }
        exit;
    }
}

/^[\t ]*index:/{if (default_found) exit;}'
