#! /bin/bash

if ~/.config/sway/is_mic_muted.sh; then
   pactl set-source-mute @DEFAULT_SOURCE@ false && paplay ~/.config/sway/unmute.ogg &
else
   pactl set-source-mute @DEFAULT_SOURCE@ true && paplay ~/.config/sway/mute.ogg &
fi
pkill -SIGRTMIN+9 waybar
